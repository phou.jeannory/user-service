package com.example.exceptions;

public class CustomSecurityContextException extends RuntimeException {

    public CustomSecurityContextException(String message) {
        super(message);
    }
}
