package com.example.exceptions;

public class CustomServiceException extends RuntimeException {

    public CustomServiceException(String message) {
        super(message);
    }
}
