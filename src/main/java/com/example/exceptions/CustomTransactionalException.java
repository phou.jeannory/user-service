package com.example.exceptions;

public class CustomTransactionalException extends RuntimeException {

    public CustomTransactionalException() {
    }

    public CustomTransactionalException(String message) {
        super(message);
    }
}
