package com.example.services;

import com.example.dtos.UserDto;

import java.util.List;

public interface UserService {
    UserDto getConnectedUserDto();
    List<UserDto> getAllUsers(Integer pageNo, Integer pageSize, String sortBy);
}
