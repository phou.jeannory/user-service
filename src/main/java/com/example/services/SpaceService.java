package com.example.services;

import com.example.dtos.SpaceDto;

import java.util.List;

public interface SpaceService {
    List<SpaceDto> getAllSpaces(Integer pageNo, Integer pageSize, String sortBy);
}
