package com.example.services;

import com.example.dtos.SpamDto;

import javax.jms.JMSException;
import javax.jms.Message;

public interface SpamService {

    SpamDto getSpamDto();
    void getSpamIncrementQueueOut(Message message) throws JMSException;
}
