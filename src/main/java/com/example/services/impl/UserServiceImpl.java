package com.example.services.impl;

import com.example.dtos.UserDto;
import com.example.entities.Space;
import com.example.entities.User;
import com.example.exceptions.CustomServiceException;
import com.example.exceptions.CustomTransactionalException;
import com.example.repositories.SpaceRepository;
import com.example.repositories.UserRepository;
import com.example.services.UserService;
import com.example.converters.SuperConverter;
import com.example.security.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private KeycloakSecurityContext keycloakSecurityContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    @Qualifier("UserUserDtoConverter")
    private SuperConverter<User, UserDto> converter;


    @Override
    public UserDto getConnectedUserDto() {
        try {
            return converter.toDto(getConnectedUser());
        } catch (CustomServiceException ex) {
            return null;
        }
    }

    @Override
    public List<UserDto> getAllUsers(final Integer pageNo, final Integer pageSize, final String sortBy) {
        final Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        final Page<User> pagedResult = userRepository.findAll(paging);
        if(pagedResult.hasContent()) {
            return converter.toDtos(pagedResult.getContent());
        } else {
            return new ArrayList<>();
        }
    }

    //catch CustomTransactionalException in this method
    private User getConnectedUser() {
        final AccessToken accessToken = keycloakSecurityContext.getAccessToken();
        if (accessToken == null) {
            throw new CustomServiceException("error getAccessToken failed");
        } else if (accessToken.getPreferredUsername() == null || accessToken.getPreferredUsername().isEmpty()) {
            throw new CustomServiceException("error preferredUsername cannot be null or empty");
        }
        final String username = accessToken.getPreferredUsername();
        User user = userRepository.findByUsernameIgnoreCase(username);
        if (user == null) {
            try {
                user = testCreateUser(accessToken);
            } catch (CustomTransactionalException ex) {
                return null;
            }
        }
        return user;
    }

    // Do not catch CustomTransactionalException in this method
    @Transactional(propagation = Propagation.REQUIRES_NEW,
            rollbackFor = CustomTransactionalException.class)
    private User testCreateUser(final AccessToken accessToken) {
        return validateCreateUser(accessToken);
    }

    // MANDATORY: Transaction must be created before.
    @Transactional(propagation = Propagation.MANDATORY)
    private User validateCreateUser(final AccessToken accessToken) {
        try {
            final Space space = new Space(null, accessToken.getPreferredUsername() + "_space", null);
            spaceRepository.save(space);
            final User user = new User();
            user.setUsername(accessToken.getPreferredUsername());
            user.setEmail(accessToken.getEmail());
            user.setFirstName(accessToken.getGivenName());
            user.setLastName(accessToken.getFamilyName());
            user.setSpace(space);
            userRepository.save(user);
            return userRepository.findByUsernameIgnoreCase(accessToken.getPreferredUsername());
        } catch (Exception ex) {
            throw new CustomTransactionalException("persistence failed : " + ex.getMessage());
        }
    }

}
