package com.example.services.impl;

import com.example.services.SpamService;
import com.example.singleton.SingletonBean;
import com.example.dtos.SpamDto;
import com.example.security.KeycloakSecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Service
public class SpamServiceImpl implements SpamService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private KeycloakSecurityContext keycloakSecurityContext;

    @Autowired
    private SingletonBean singletonBean;

    @Override
    public SpamDto getSpamDto() {
        final SpamDto spamDto = new SpamDto();
        spamDto.setConnected(keycloakSecurityContext.isConnected());
        sendSpam(spamDto);
        return spamDto;
    }

    @Override
    @JmsListener(destination = "spamIncrementQueueOut", containerFactory = "myFactory")
    public void getSpamIncrementQueueOut(Message message) throws JMSException {
        final SpamDto spamDto = singletonBean.getModelMapper().map(((TextMessage) message).getText(), SpamDto.class);
        System.out.println("receive from MOM " + spamDto);
    }

    private void sendSpam(SpamDto spamDto) {
        try {
            jmsTemplate.convertAndSend("spamIncrement", spamDto);
            jmsTemplate.setReceiveTimeout(10_000);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }

}
