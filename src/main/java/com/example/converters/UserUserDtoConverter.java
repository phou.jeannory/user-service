package com.example.converters;

import com.example.entities.Space;
import com.example.entities.User;
import com.example.dtos.SpaceDto;
import com.example.dtos.UserDto;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

@Service("UserUserDtoConverter")
public class UserUserDtoConverter<E, D> extends SuperConverter<E, D> {

    public UserUserDtoConverter() {
        super(User.class,UserDto.class);
    }

    @Override
    public D toDto(E entity) {
        final User user = (User) entity;
        final UserDto userDto = getSingletonBean().getModelMapper().map(user, (Type) getDtoClass());
        if(user.getSpace()!=null){
            final SpaceDto spaceDto = getSingletonBean().getModelMapper().map(user.getSpace(), (Type) SpaceDto.class);
            userDto.setSpace(spaceDto);
        }
        return (D) userDto;
    }

    @Override
    public E toEntity(D dto) {
        final UserDto userDto = (UserDto) dto;
        final User user = getSingletonBean().getModelMapper().map(userDto, (Type) getEntityClass());
        if(userDto.getSpace()!=null){
            final Space space = getSingletonBean().getModelMapper().map(userDto.getSpace(), (Type) Space.class);
            user.setSpace(space);
        }
        return (E) user;
    }

}
