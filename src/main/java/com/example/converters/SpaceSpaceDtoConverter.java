package com.example.converters;

import com.example.entities.Space;
import com.example.dtos.SpaceDto;
import org.springframework.stereotype.Service;

@Service("SpaceSpaceDtoConverter")
public class SpaceSpaceDtoConverter<E, D> extends SuperConverter<E, D>  {

    public SpaceSpaceDtoConverter() {
        super(Space.class, SpaceDto.class);
    }
}
