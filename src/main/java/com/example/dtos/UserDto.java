package com.example.dtos;

import com.example.enums.Gender;
import com.example.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends SuperDto {

    private String username;
    private String email;
    private Gender gender;
    private String firstName;
    private String lastName;
    private Status status;
    private SpaceDto space;
}
