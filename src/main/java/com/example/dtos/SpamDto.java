package com.example.dtos;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class SpamDto {

    private boolean isConnected;
    private String now;

    public SpamDto() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        now = formatter.format(date);
    }

}
