package com.example.controllers;

import com.example.dtos.SpamDto;
import com.example.services.SpamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("api/spam")
public class SpamController {

    @Autowired
    private SpamService spamService;

    //http://localhost:8081/api/spam/getSpam
    @RequestMapping(value = "/get-spam")
    public ResponseEntity<SpamDto> getSpam() {
        return new ResponseEntity<SpamDto>(spamService.getSpamDto(), new HttpHeaders(), HttpStatus.OK);
    }
}
