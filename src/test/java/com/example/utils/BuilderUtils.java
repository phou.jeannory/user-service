package com.example.utils;

import com.example.dtos.SpaceDto;
import com.example.dtos.UserDto;
import com.example.entities.Space;
import com.example.entities.User;
import com.example.enums.Gender;
import com.example.enums.Status;
import org.keycloak.representations.AccessToken;

public class BuilderUtils {

    public static Space getSpace(){
        return new Space(1L, "user1_space", null);
    }

    public static SpaceDto getSpaceDto(){
        final SpaceDto spaceDto = new SpaceDto("user1_space");
        spaceDto.setId(1L);
        return spaceDto;
    }

    public static User getUser(){
        return new User(1L, "user1", "user1@gmail.com", Gender.Mister, "firstName1", "name1", Status.ACTIVE, getSpace());
    }

    public static UserDto getUserDto(){
        final UserDto userDto = new UserDto("user1", "user1@gmail.com", Gender.Mister, "firstName1", "name1", Status.ACTIVE, getSpaceDto());
        userDto.setId(1L);
        return userDto;
    }

    public static AccessToken getAccessToken(){
        final AccessToken accessToken = new AccessToken();
        accessToken.setPreferredUsername("user1");
        return accessToken;
    }

}
