package com.example.controllers;

import com.example.utils.BuilderUtilsKeycloak;
import com.example.utils.BuilderUtilsResultAction;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Initialize spring context before the class
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
/**
 * ActiveProfiles must be launch by maven
 * mvn test -Pdev-local -Dspring.profiles.active=dev-local
 */
//@ActiveProfiles("dev-docker")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class SpaceControllerContextTest extends BuilderUtilsResultAction {

    @Autowired
    private BuilderUtilsKeycloak builderUtilsKeycloak;

    @Test
    public void getSpacesShouldForbiddenWhenRoleIsUser() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("test", "1234");
        final MvcResult mvcResult = invokeGet("/api/spaces", accessToken)
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void getSpacesShouldReturnSize10WhenRoleIsManager() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("phou.jeannory@gmail.com", "1234");
        final MvcResult mvcResult = invokeGet("/api/spaces", accessToken)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(10)))
                .andExpect(jsonPath("$[0].name", is("user1_space")))
                .andReturn();
    }

    @Test
    public void getSpacesShouldReturnSize5WhenRoleIsManager() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("phou.jeannory@gmail.com", "1234");
        final MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.put("pageNo", Collections.singletonList("0"));
        parameters.put("pageSize", Collections.singletonList("5"));
        parameters.put("sortBy", Collections.singletonList("name"));
        final MvcResult mvcResult = invokeGet("/api/spaces", accessToken, parameters)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(5)))
                .andExpect(jsonPath("$[0].name", is("john.doe@gmail.com_space")))
                .andReturn();
    }
}