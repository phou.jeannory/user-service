package com.example.controllers;

import com.example.utils.BuilderUtilsKeycloak;
import com.example.utils.BuilderUtilsResultAction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Initialize spring context before the class
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
/**
 * ActiveProfiles must be launch by maven
 * mvn test -Pdev-local -Dspring.profiles.active=dev-docker
 */
//@ActiveProfiles("dev-docker")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class UserControllerContextTest extends BuilderUtilsResultAction {
    @Autowired
    private BuilderUtilsKeycloak builderUtilsKeycloak;

    @Test
    public void getGetUsersShouldReturnForbiddenWhenRoleIsUser() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("test", "1234");
        final MvcResult mvcResult = invokeGet("/api/users", accessToken)
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testGetUsersWhenRoleIsManagerShouldReturn9() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("phou.jeannory@gmail.com", "1234");
        final MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.put("pageNo", Collections.singletonList("0"));
        parameters.put("pageSize", Collections.singletonList("9"));
        parameters.put("sortBy", Collections.singletonList("id"));
        final MvcResult mvcResult = invokeGet("/api/users", accessToken, parameters)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(9)))
                .andExpect(jsonPath("$[0].username", is("user1")))
                .andReturn();
    }

    @Test
    public void testGetConnectedUser() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("test", "1234");
        final MvcResult mvcResult = invokeGet("/api/users/connected-user", accessToken)
                .andExpect(status().isOk())
                .andExpect(jsonPath("email", is("test@gmail.com")))
                .andExpect(jsonPath("space.name", is("test_space")))
                .andReturn();
    }

}