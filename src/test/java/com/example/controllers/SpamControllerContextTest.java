package com.example.controllers;

import com.example.utils.BuilderUtilsKeycloak;
import com.example.utils.BuilderUtilsResultAction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Initialize spring context before the class
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
/**
 * ActiveProfiles must be launch by maven
 * mvn test -Pdev-local -Dspring.profiles.active=dev-docker
 */
//@ActiveProfiles("dev-docker")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class SpamControllerContextTest extends BuilderUtilsResultAction {

    @Autowired
    private BuilderUtilsKeycloak builderUtilsKeycloak;

    @Test
    public void getSpamShouldReturnTrueWhenUserIsConnected() throws Exception {
        final String accessToken = builderUtilsKeycloak.getAccessToken("test", "1234");
        final MvcResult mvcResult = invokeGet("/api/spam/get-spam", accessToken)
                .andExpect(status().isOk())
                .andExpect(jsonPath("connected", is(true)))
                .andReturn();
    }

    @Test
    public void getSpamShouldReturnFalseWhenUserIsNotConnected() throws Exception {
        final MvcResult mvcResult = invokeGet("/api/spam/get-spam")
                .andExpect(status().isOk())
                .andExpect(jsonPath("connected", is(false)))
                .andReturn();
    }
}