package com.example.services.impl;

import com.example.repositories.SpaceRepository;
import com.example.converters.SuperConverter;
import com.example.dtos.SpaceDto;
import com.example.entities.Space;
import com.example.entities.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SpaceServiceImplTest {

    @InjectMocks
    private SpaceServiceImpl spaceService;

    @Mock
    private SpaceRepository spaceRepository;

    @Mock
    private SuperConverter<Space, SpaceDto> converter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllSpacesShouldReturnSpaces() {

        //given
        final Pageable paging = PageRequest.of(0, 10, Sort.by("id"));
        final User user1 = new User();
        user1.setId(1L);
        user1.setEmail("jean1@gmail.com");
        final User user2 = new User();
        user2.setId(2L);
        user2.setEmail("jean2@gmail.com");
        final List<Space> spaces = Arrays.asList(new Space(1L, "space1", user1), new Space(2L, "space2", user2));
        final Page<Space> pagedResult = new PageImpl(spaces);
        Mockito.when(spaceRepository.findAll(paging)).thenReturn(pagedResult);
        final List<SpaceDto> spaceDtos = Arrays.asList(new SpaceDto("space1"), new SpaceDto("space2"));
        Mockito.when(converter.toDtos(pagedResult.getContent())).thenReturn(spaceDtos);

        //when
        final List<SpaceDto> results = spaceService.getAllSpaces(0, 10, "id");

        //then
        Assert.assertEquals(results.size(), 2);
    }

    @Test
    public void testGetAllSpacesShouldReturnEmptyList() {

        //given
        final Pageable paging = PageRequest.of(0, 10, Sort.by("id"));
        final List<Space> spaces = Collections.emptyList();
        final Page<Space> pagedResult = new PageImpl(spaces);
        Mockito.when(spaceRepository.findAll(paging)).thenReturn(pagedResult);
        final List<SpaceDto> spaceDtos = Collections.emptyList();
        Mockito.when(converter.toDtos(pagedResult.getContent())).thenReturn(spaceDtos);

        //when
        final List<SpaceDto> results = spaceService.getAllSpaces(0, 10, "id");

        //then
        Assert.assertEquals(results, Collections.emptyList());
    }

}