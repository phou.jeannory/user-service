package com.example.converters;

import com.example.dtos.SpaceDto;
import com.example.entities.Space;
import com.example.singleton.SingletonBean;
import com.example.utils.BuilderUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.modelmapper.ModelMapper;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SpaceSpaceDtoConverterTest {

    private SuperConverter<Space, SpaceDto> converter;

    private SingletonBean singletonBean;

    @Before
    public void setUp() throws Exception {
        this.converter = new SpaceSpaceDtoConverter<Space, SpaceDto>();
        singletonBean = Mockito.mock(SingletonBean.class);
        Whitebox.setInternalState(converter, "singletonBean", singletonBean);
    }

    @Test
    public void toDto() {

        //given
        final Space space = new Space();
        space.setName("john_space");
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final SpaceDto result = converter.toDto(space);

        //then
        assertEquals("john_space", result.getName());
    }

    @Test
    public void toDtoBis() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final SpaceDto result = converter.toDto(BuilderUtils.getSpace());

        //then
        assertEquals(1L, (long) result.getId());
        assertEquals("user1_space", result.getName());
    }

    @Test
    public void toDtos() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final List<SpaceDto> results = converter.toDtos(Collections.singletonList(BuilderUtils.getSpace()));

        //then
        assertEquals(1, (int) results.size());
        assertEquals("user1_space", results.get(0).getName());
    }

    @Test
    public void toEntity() {
        //given
        final SpaceDto spaceDto = new SpaceDto();
        spaceDto.setName("john_space");
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final Space result = converter.toEntity(spaceDto);

        //then
        assertEquals("john_space", result.getName());
    }

    @Test
    public void toEntityBis() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final Space result = converter.toEntity(BuilderUtils.getSpaceDto());

        //then
        assertEquals(1L, (long) result.getId());
        assertEquals("user1_space", result.getName());
        assertNull("return null", result.getUser());
    }

    @Test
    public void toEntities() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final List<Space> results = converter.toEntities(Collections.singletonList(BuilderUtils.getSpaceDto()));

        //then
        assertEquals(1, (int) results.size());
        assertEquals("user1_space", results.get(0).getName());
        assertNull("return null", results.get(0).getUser());
    }
}