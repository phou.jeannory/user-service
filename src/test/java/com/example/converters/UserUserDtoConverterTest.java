package com.example.converters;

import com.example.entities.Space;
import com.example.entities.User;
import com.example.enums.Gender;
import com.example.singleton.SingletonBean;
import com.example.utils.BuilderUtils;
import com.example.dtos.SpaceDto;
import com.example.dtos.UserDto;
import com.example.enums.Status;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserUserDtoConverterTest {

    @InjectMocks
    private SuperConverter<User,UserDto> converter;

    @Mock
    private SingletonBean singletonBean;

    @Before
    public void setUp() {
        this.converter = new UserUserDtoConverter<User,UserDto>();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testToDto() {
        //given
        final User user = Mockito.spy(new User());
        Mockito.when(user.getUsername()).thenReturn("john");
        final Space space = new Space();
        space.setName("john_space");
        user.setSpace(space);
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final UserDto result = converter.toDto(user);

        //then
        assertEquals("john",result.getUsername());
        assertEquals("john_space", result.getSpace().getName());
    }

    @Test
    public void testToDtoBis() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final UserDto result = converter.toDto(BuilderUtils.getUser());

        //then
        assertEquals(1L, (long) result.getId());
        assertEquals("user1",result.getUsername());
        assertEquals("user1@gmail.com",result.getEmail());
        assertEquals(Gender.Mister,result.getGender());
        assertEquals("firstName1",result.getFirstName());
        assertEquals("name1",result.getLastName());
        assertEquals(Status.ACTIVE,result.getStatus());
        assertEquals("user1_space", result.getSpace().getName());
    }

    @Test
    public void toDtos() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final List<UserDto> results = converter.toDtos(Collections.singletonList(BuilderUtils.getUser()));

        //then
        assertEquals(1, (int) results.size());
        assertEquals("user1",results.get(0).getUsername());
        assertEquals("user1@gmail.com",results.get(0).getEmail());
        assertEquals(Gender.Mister,results.get(0).getGender());
        assertEquals("firstName1",results.get(0).getFirstName());
        assertEquals("name1",results.get(0).getLastName());
        assertEquals(Status.ACTIVE,results.get(0).getStatus());
        assertEquals("user1_space", results.get(0).getSpace().getName());
    }

    @Test
    public void toEntity() {
        //given
        final SpaceDto spaceDto = new SpaceDto();
        spaceDto.setName("john_space");
        final UserDto userDto = new UserDto();
        userDto.setUsername("john");
        userDto.setSpace(spaceDto);
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final User result = converter.toEntity(userDto);

        //then
        assertEquals("john",result.getUsername());
        assertEquals("john_space", result.getSpace().getName());
    }

    @Test
    public void toEntityBis() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final User result = converter.toEntity(BuilderUtils.getUserDto());

        //then
        assertEquals(1L, (long) result.getId());
        assertEquals("user1",result.getUsername());
        assertEquals("user1@gmail.com",result.getEmail());
        Assert.assertEquals(Gender.Mister,result.getGender());
        assertEquals("firstName1",result.getFirstName());
        assertEquals("name1",result.getLastName());
        assertEquals(Status.ACTIVE,result.getStatus());
        assertEquals("user1_space", result.getSpace().getName());
        assertNull("return null", result.getSpace().getUser());
    }

    @Test
    public void toEntities() {
        //given
        Mockito.when(singletonBean.getModelMapper()).thenReturn(new ModelMapper());

        //when
        final List<User> results = converter.toEntities(Collections.singletonList(BuilderUtils.getUserDto()));

        //then
        assertEquals(1, (int) results.size());
        assertEquals("user1",results.get(0).getUsername());
        assertEquals("user1@gmail.com",results.get(0).getEmail());
        Assert.assertEquals(Gender.Mister,results.get(0).getGender());
        assertEquals("firstName1",results.get(0).getFirstName());
        assertEquals("name1",results.get(0).getLastName());
        assertEquals(Status.ACTIVE,results.get(0).getStatus());
        assertEquals("user1_space", results.get(0).getSpace().getName());
        assertNull("return null", results.get(0).getSpace().getUser());
    }
}