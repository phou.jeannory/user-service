FROM java:8
ARG JAR_FILE=target/user-service.jar
ARG JAR_LIB_FILE=target/lib/

# cd /usr/local/runme
WORKDIR /usr/local/runme

# cp target/user-service.jar /usr/local/runme/user-service.jar
COPY ${JAR_FILE} user-service.jar

# cp -rf target/lib/  /usr/local/runme/lib
ADD ${JAR_LIB_FILE} lib/

# java -jar /usr/local/runme/user-service.jar
ENTRYPOINT ["java","-jar","user-service.jar"]